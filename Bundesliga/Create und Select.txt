﻿CREATE TABLE IF NOT EXISTS Schiedsrichter(
zulassungsnummer INT PRIMARY KEY,
vorname TEXT NOT NULL,
nachname TEXT NOT NULL);

CREATE TABLE IF NOT EXISTS Stadion(
name TEXT PRIMARY KEY,
ort TEXT NOT NULL,
plz TEXT NOT NULL,
straße TEXT NOT NULL,
hnr TEXT NOT NULL,
plätze INT NOT NULL);

CREATE TABLE IF NOT EXISTS Spieler(
spielerPassNr INT PRIMARY KEY,
vorname TEXT NOT NULL,
nachname TEXT NOT NULL,
gebDatum DATE NOT NULL);

CREATE TABLE IF NOT EXISTS Team(
Vereinsname TEXT PRIMARY KEY,
ort TEXT NOT NULL,
plz TEXT NOT NULL,
straße TEXT NOT NULL,
hnr TEXT NOT NULL);

CREATE TABLE IF NOT EXISTS Spiel(
spielID INT PRIMARY KEY,
heimMannschaft TEXT NOT NULL REFERENCES Team(vereinsname),
gästeMannschaft TEXT NOT NULL REFERENCES Team(vereinsname),
toreHeimMannschaft INT NOT NULL,
toreGästeMannschaft INT NOT NULL,
schiedsrichter INT NOT NULL REFERENCES Schiedsrichter(zulassungsNummer),
linienrichter1 INT NOT NULL REFERENCES Schiedsrichter(zulassungsNummer),
linienrichter2 INT NOT NULL REFERENCES Schiedsrichter(zulassungsNummer),
stadion TEXT NOT NULL REFERENCES Stadion(name),
datum DATE NOT NULL);


CREATE TABLE IF NOT EXISTS SpieltFür(
spielerPassNr INT NOT NULL REFERENCES Spieler(spielerPassNr),
vereinsname TEXT NOT NULL REFERENCES Team(vereinsname),
von DATE NOT NULL,
bis DATE DEFAULT 0,
PRIMARY KEY(spielerPassNr, vereinsname));

CREATE TABLE IF NOT EXISTS Karten(
spielerPassNr INT NOT NULL REFERENCES Spieler(spielerPassNr),
spielID INT NOT NULL REFERENCES Spiel(spielID),
spielminute INT NOT NULL,
farbe TEXT NOT NULL,
PRIMARY KEY(spielerPassNr, spielID,spielminute));

CREATE TABLE IF NOT EXISTS Tor(
spielerPassNr INT NOT NULL REFERENCES Spieler(spielerPassNr),
spielID INT NOT NULL REFERENCES Spiel(spielID),
spielminute INT NOT NULL,
PRIMARY KEY(spielerPassNr, spielID,spielminute));


--meiste Heimtore
SELECT vorname, nachname 
FROM Tor t, Spiel s, SpieltFür sf, Spieler sp 
WHERE t.spielID=s.spielID AND s.heimMannschaft=sf.vereinsname AND sf.bis=0 AND sf.spielerPassNr=t.spielerPassNr AND sp.spielerPassNr=t.spielerPassNr 
GROUP BY t.spielerPassNr  
ORDER BY count(t.spielerPassNr) desc LIMIT 1;  

--öftester wechsel
SELECT vorname, nachname FROM Spieler s, SpieltFür sf WHERE s.spielerPassNr=sf.spielerPassNr GROUP BY sf.spielerPassNr ORDER BY count(sf.spielerPassNr) desc LIMIT 1;

--schiedsrichter mit meisten roten karten
SELECT vorname, nachname FROM Karten k, Spiel s,Schiedsrichter sc  WHERE zulassungsnummer=schiedsrichter AND k.spielID=s.spielID AND farbe="Rot" GROUP BY sc.zulassungsnummer ORDER BY count(sc.zulassungsnummer) desc LIMIT 1;

--summe tore
SELECT count() FROM Tor;

--durchschnittstore
SELECT CAST(COUNT() as REAL)/(SELECT CAST(COUNT() as REAL) FROM Spiel) FROM Tor;

--meist karten erhalten
SELECT vorname, nachname FROM Spieler s, Karten k WHERE s.spielerPassNr=k.spielerPassNr  GROUP BY k.spielerPassNr ORDER BY count(k.spielerPassNr) desc LIMIT 1;

--Torstärkste woche
SELECT strftime("%Y%W", datum) as week, sum(toreGästeMannschaft+toreHeimMannschaft) as tore FROM Spiel GROUP BY week ORDER BY tore desc LIMIT 1;

--Tore pro saison
SELECT strftime("%Y", datum) as saison, sum(toreGästeMannschaft+toreHeimMannschaft) as tore FROM Spiel GROUP BY saison;
