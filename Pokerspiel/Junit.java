import static org.junit.Assert.*;

import org.junit.Test;

public class Junit {

	@Test
	public void testEinPaar() {
		boolean b = true;
		boolean a;
		int[] anzahlhand = new int[5];
		
		a = Main.einPaar(anzahlhand);
		assertEquals(a, b);
	}

	@Test
	public void testZweiPaare() {
		boolean b = false;
		boolean a;
		boolean c = false;
		int[] anzahlhand = new int[5];
		a = Main.zweiPaare(anzahlhand);
		assertEquals(a, b);
		assertEquals(a, c);
	}

	

	@Test
	public void testDrilling() {
		boolean b = true;
		boolean a;
		int[] anzahlhand = new int[5];
		
		a = Main.drilling(anzahlhand);
		assertEquals(a, b);
	}

	@Test
	public void testVierling() {
		boolean b = true;
		boolean a;
		int[] anzahlhand = new int[5];
		
		a = Main.vierling(anzahlhand);
		assertEquals(a, b);
	}

	@Test
	public void testFlush() {
		boolean b = true;
		boolean a;
		int[] anzahlhand = new int[5];
		
		a = Main.flush(anzahlhand);
		assertEquals(a, b);
	}

	@Test
	public void testStraight() {
		boolean b = false;
		boolean a;
		int[] anzahlhand = new int[5];
		
		a = Main.straight(anzahlhand);
		assertEquals(a, b);
	}

	@Test
	public void testStraightFlush() {
		boolean b = false;
		boolean a;
		boolean c = false;
		int[] anzahlhand = new int[5];
		a = Main.straightFlush(anzahlhand);
		assertEquals(a, b);
		assertEquals(a, c);
	}

	@Test
	public void testFullHouse() {
		boolean b = true;
		boolean a;
		boolean c = true;
		int[] anzahlhand = {33,33,33,22,22};
		a = Main.fullHouse(anzahlhand);
		assertEquals(a, b);
		assertEquals(a, c);
	}

}
