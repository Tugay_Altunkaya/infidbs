import java.util.Arrays;
import java.sql.*;
public class Main {
	final static int ANZAHLGLEICHERFARBEN = 13;
	final static int ANZAHLFARBEN = 4;
	static double[] a = new double[8];
	static int[] anzahlhand = new int[5];
  
	public static void main(String[] args) {
        long stamp1 = System.currentTimeMillis();
		for (int i = 0; i < 2000000; i++) {
			ziehen();
			if (einPaar(anzahlhand)) {
				a[0]++;
			}
			if (zweiPaare(anzahlhand)) {
				a[1]++;
			}
			if (drilling(anzahlhand)) {
				a[2]++;
			}
			if (vierling(anzahlhand)) {
				a[3]++;
			}
			if (flush(anzahlhand)) {
				a[4]++;
			}
			if (fullHouse(anzahlhand)) {
				a[5]++;
			}
			if (straight(anzahlhand)) {
				a[6]++;
			}
			if (straightFlush(anzahlhand)) {
				a[7]++;
			}

		}
		
		
		
		long stamp2 = System.currentTimeMillis();
		long dauer = stamp2 - stamp1;
		System.out.println("Dauer in millisekunden: " + dauer);
		System.out.println("Diese werte liegen vor");
		System.out.println("ein Paar: " + a[0] / 2000000 * 100 + "%");
		System.out.println("zwei Paare: " + a[1] / 2000000 * 100 + "%");
		System.out.println("Drilling: " + a[2] / 2000000 * 100 + "%");
		System.out.println("Vierling: " + a[3] / 2000000 * 100 + "%");
		System.out.println("Flush: " + a[4] / 2000000 * 100 + "%");
		System.out.println("Full House: " + a[5] / 2000000 * 100 + "%");
		System.out.println("Straight: " + a[6] / 2000000 * 100 + "%");
		System.out.println("Straight Flush: " + a[7] / 2000000 * 100 + "%");
		
		
		 Connection c = null;
		    Statement stmt = null;
		    try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:test.db");
		      System.out.println("Opened database successfully");

		      stmt = c.createStatement();
		      String sql = "CREATE TABLE IF NOT EXISTS Fivecardgame2 " +
	                  "(time INT PRIMARY KEY     ," +
	                  " einPaar            INT     NOT NULL, " + 
	                  " zweiPaare            INT     NOT NULL, " + 
	                  " drilling            INT     NOT NULL," +
	                  " vierling            INT     NOT NULL," +
	                  " flush            INT     NOT NULL," +
	                  " fullHouse            INT     NOT NULL," +
	                  " straight            INT     NOT NULL," +
	                  " straightFlush            INT     NOT NULL)";
	                 
	                  
		      stmt.executeUpdate(sql);
		      stmt.close();
		     
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		    System.out.println("Table created successfully");
		  
		
		 
		  
		    try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:test.db");
		      c.setAutoCommit(false);
		      System.out.println("Opened database successfully");

		      PreparedStatement preparedStatement = null;
		      String sql = "INSERT INTO Fivecardgame2 Values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		      
		      preparedStatement = c.prepareStatement(sql);
		      preparedStatement.setLong(1, dauer);
		      preparedStatement.setDouble(2, a[0]);
		      preparedStatement.setDouble(3, a[1]);
		      preparedStatement.setDouble(4, a[2]);
		      preparedStatement.setDouble(5, a[3]);
		      preparedStatement.setDouble(6, a[4]);
		      preparedStatement.setDouble(7, a[5]);
		      preparedStatement.setDouble(8, a[6]);
		      preparedStatement.setDouble(9, a[7]);
		      
		      stmt.executeUpdate(sql);

		     
		      stmt.close();
		      c.commit();
		      c.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		    System.out.println("Created successfully");
		
		
		
	}

	public static void ziehen() {
		for (int i = 0; i < 5; i++) {

			int[] anzahlkarten = new int[ANZAHLGLEICHERFARBEN * ANZAHLFARBEN];
			int random = (int) (Math.random() * (52 - i));
			for (int k = 0; k < 52; k++) {
				anzahlkarten[k] = k + 1;
			}
			anzahlhand[i] = anzahlkarten[random];
			int merke = anzahlkarten[51 - i];
			anzahlkarten[random] = anzahlkarten[51 - 1];
			anzahlkarten[random] = merke;
		}
	}

	public static int wahrerWertDerKarte(int karte) {
		return karte % ANZAHLGLEICHERFARBEN;

	}

	public static int farbe(int karte) {
		return karte / ANZAHLFARBEN;
	}

	public static boolean einPaar(int[] anzahlhand) {
		for (int i = 0; i < anzahlhand.length - 1; i++) {
			for (int j = (i + 1); j < anzahlhand.length; j++) {
				if (wahrerWertDerKarte(anzahlhand[i]) == wahrerWertDerKarte(anzahlhand[j])) {
					return true;
				}
			}
		}
		return false;

	}

	public static boolean zweiPaare(int[] anzahlhand) {
		int paare = 0;
		boolean a = false;
		boolean b = false;
		for (int i = 0; i < anzahlhand.length - 1; i++) {
			for (int j = (i + 1); j < anzahlhand.length; j++) {
				if (wahrerWertDerKarte(anzahlhand[i]) == wahrerWertDerKarte(anzahlhand[j])) {
					paare = wahrerWertDerKarte(anzahlhand[i]);
					a = true;
				}
			}
		}
		for (int i = 0; i < anzahlhand.length - 1; i++) {
			for (int j = (i + 1); j < anzahlhand.length; j++) {
				if (wahrerWertDerKarte(anzahlhand[i]) == wahrerWertDerKarte(anzahlhand[j])
						&& !(wahrerWertDerKarte(anzahlhand[i]) == paare)) {
					b = true;
				}
			}
		}
		return a && b;
	}

	public static boolean drilling(int[] anzahlhand) {
		for (int i = 0; i < anzahlhand.length - 2; i++) {
			for (int j = (i + 1); j < anzahlhand.length - 1; j++) {
				for (int c = (i + 2); c < anzahlhand.length; c++) {

					if (wahrerWertDerKarte(anzahlhand[i]) == wahrerWertDerKarte(anzahlhand[j])
							&& wahrerWertDerKarte(anzahlhand[i]) == wahrerWertDerKarte(anzahlhand[c])) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean vierling(int[] anzahlhand) {
		for (int i = 0; i < anzahlhand.length - 3; i++) {
			for (int j = (i + 1); j < anzahlhand.length - 2; j++) {
				for (int c = (i + 2); c < anzahlhand.length - 1; c++) {
					for (int k = (i + 3); k < anzahlhand.length; k++) {

						if (wahrerWertDerKarte(anzahlhand[i]) == wahrerWertDerKarte(anzahlhand[j])
								&& wahrerWertDerKarte(anzahlhand[i]) == wahrerWertDerKarte(anzahlhand[c])
								&& wahrerWertDerKarte(anzahlhand[i]) == wahrerWertDerKarte(anzahlhand[k])) {
							return true;
						}
					}
				}
			}
		}
		return false;

	}

	public static boolean flush(int[] anzahlhand) {
		int f = farbe(anzahlhand[0]);

		for (int i = 0; i < 5; i++) {
			if (f != farbe(anzahlhand[i])) {
				return false;
			}
		}
		return true;

	}

	public static boolean straight(int[] anzahlhand) {
		Arrays.sort(anzahlhand);
		for (int i = 1; i < anzahlhand.length-1; i++) {
			if (anzahlhand[i] - anzahlhand[i - 1] != 1) {
				return false;
			}
		}
		return true;
	}

	public static boolean straightFlush(int[] anzahlhand) {
		Arrays.sort(anzahlhand);
		boolean a = true;
		boolean b = true;
		int f = farbe(anzahlhand[0]);
		for (int i = 1; i < anzahlhand.length; i++) {
			if (anzahlhand[i] - anzahlhand[i - 1] != 1) {
				a = false;
			}
		}
		for (int i = 0; i < 5; i++) {
			if (f != anzahlhand[i]) {
				b = false;
			}
		}
		return a && b;

	}

	public static boolean fullHouse(int[] anzahlhand) {
		int drilling = 0;
		boolean a = false;
		boolean b = false;
		for (int i = 0; i < anzahlhand.length - 2; i++) {
			for (int j = (i + 1); j < anzahlhand.length - 1; j++) {
				for (int c = (i + 2); c < anzahlhand.length; c++) {

					if (wahrerWertDerKarte(anzahlhand[i]) == wahrerWertDerKarte(anzahlhand[j])
							&& wahrerWertDerKarte(anzahlhand[j]) == wahrerWertDerKarte(anzahlhand[c])) {
						drilling = wahrerWertDerKarte(anzahlhand[i]);
						a = true;
					}
				}
			}
		}

		for (int i = 0; i < anzahlhand.length - 1; i++) {
			for (int j = (i + 1); j < anzahlhand.length; j++) {
				if (wahrerWertDerKarte(anzahlhand[i]) == wahrerWertDerKarte(anzahlhand[j])
						&& wahrerWertDerKarte(anzahlhand[i]) != drilling) {

					b = true;
				}
			}
		}
		return a && b;
	}

}
